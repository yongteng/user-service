package app

import (
	"user-service/microservice/accounts"
	"user-service/microservice/controllers"

	"github.com/gin-gonic/gin"
)

func mapURL() {
	account := accounts.GetJSONInfo()
	user := gin.Accounts{account.User: account.Password}

	authorized := router.Group("/v1", gin.BasicAuth(user))
	authorized.POST("/users", controllers.UsersController.Create)
	authorized.GET("/users/:id", controllers.UsersController.Get)
	authorized.PUT("/users/:id", controllers.UsersController.Update)
	authorized.DELETE("/users/:id", controllers.UsersController.Delete)
}
