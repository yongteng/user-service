package controllers

import (
	"fmt"
	"net/http"
	"strconv"
	"user-service/microservice/aws"
	"user-service/microservice/domain/httperrors"
	"user-service/microservice/domain/users"
	"user-service/microservice/services"

	"github.com/gin-gonic/gin"
)

var (
	// UsersController ...
	UsersController = usersController{}
)

type usersController struct{}

func respond(c *gin.Context, isXML bool, httpCode int, body interface{}) {
	if isXML {
		c.XML(httpCode, body)
		return
	}
	c.JSON(httpCode, body)
}

// Get ...
func (controller usersController) Get(c *gin.Context) {
	isXML := c.GetHeader("Accept") == "application/xml"
	_, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		httpError := httperrors.NewBadRequestError("invalid user id")
		respond(c, isXML, httpError.Code, httpError)
		return
	}

	user := aws.Get(c)

	// user, getErr := services.UsersService.Get(userID)

	// if getErr != nil {
	// 	respond(c, isXML, getErr.Code, getErr)
	// 	return
	// }

	respond(c, isXML, http.StatusOK, user)
}

// Create ...
func (controller usersController) Create(c *gin.Context) {
	// bytes, err := ioutil.ReadAll(c.Request.Body)
	// if err != nil {
	// 	// response bad request
	// 	return
	// }

	// var user User
	// if err := json.Unmarshal(bytes, &user); err != nil {
	// 	// response bad request
	// 	return
	// }

	// The paragraph is as the same as the above
	// The main goal is that check whether the request body is ok
	var user users.User
	if err := c.ShouldBindJSON(&user); err != nil {
		// return bad request
		httpError := httperrors.NewBadRequestError("invalid json body")
		c.JSON(httpError.Code, httpError)
		return
	}
	//

	createdUser, err := services.UsersService.Create(user)
	if err != nil {
		c.JSON(err.Code, err)
		return
	}

	// return created user
	c.JSON(http.StatusCreated, createdUser)

}

func (controller usersController) Update(c *gin.Context) {
	isXML := c.GetHeader("Accept") == "application/xml"
	var user users.User
	if err := c.ShouldBindJSON(&user); err != nil {
		httpError := httperrors.NewBadRequestError("invalid json body")
		c.JSON(httpError.Code, httpError)
		return
	}

	paramID, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		httpError := httperrors.NewBadRequestError("invalid user id")
		respond(c, isXML, httpError.Code, httpError)
		return
	}

	if paramID != user.ID {
		httpError := httperrors.NewNotFouncError(fmt.Sprintf("user %d not found", user.ID))
		respond(c, isXML, httpError.Code, httpError)
		return
	}

	if err := services.UsersService.Update(user); err != nil {
		c.JSON(err.Code, err)
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": fmt.Sprintf("Update user %d succeed", user.ID)})

}

func (controller usersController) Delete(c *gin.Context) {
	isXML := c.GetHeader("Accept") == "application/xml"
	userID, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		httpError := httperrors.NewBadRequestError("invalid user id")
		respond(c, isXML, httpError.Code, httpError)
		return
	}

	getErr := services.UsersService.Delete(userID)

	if getErr != nil {
		respond(c, isXML, getErr.Code, getErr)
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": fmt.Sprintf("Delete user %d succeed", userID)})
}
