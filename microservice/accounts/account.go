package accounts

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
)

// Account ...
type Account struct {
	User     string `json:"user"`
	Password string `json:"password"`
	Email    string `json:"email"`
}

// GetJSONInfo ...
func GetJSONInfo() Account {
	absPath, _ := filepath.Abs("../microservice/accounts/account.json")

	// Open our jsonFile
	jsonFile, err := os.Open(absPath)
	// if we os.Open returns an error then handle it
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println("Successfully Opened accounts.json")
	// defer the closing of our jsonFile so that we can parse it later on
	defer jsonFile.Close()

	// read our opened xmlFile as a byte array.
	byteValue, _ := ioutil.ReadAll(jsonFile)
	fmt.Println(string(byteValue))

	// we initialize our Users array
	var account Account

	// we unmarshal our byteArray which contains our
	// jsonFile's content into 'users' which we defined above
	json.Unmarshal(byteValue, &account)

	return account
}
