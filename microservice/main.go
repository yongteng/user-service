package main

import (
	"user-service/microservice/app"
)

func main() {
	app.StartApplication()
}
