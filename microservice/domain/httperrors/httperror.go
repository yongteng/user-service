package httperrors

import "net/http"

// HTTPError ...
type HTTPError struct {
	Message string `json:"message"`
	Code    int    `json:"code"`
	Error   string `json:"error"`
}

// NewBadRequestError ...
func NewBadRequestError(message string) *HTTPError {
	return &HTTPError{
		Message: message,
		Code:    http.StatusBadRequest,
		Error:   "bad_request",
	}
}

// NewNotFouncError ...
func NewNotFouncError(message string) *HTTPError {
	return &HTTPError{
		Message: message,
		Code:    http.StatusNotFound,
		Error:   "not_found",
	}
}
