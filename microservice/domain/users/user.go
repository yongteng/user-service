package users

import "user-service/microservice/domain/httperrors"

// User ...
type User struct {
	ID        int64  `json:"id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Email     string `json:"email"`
}

// Validate ...
func (user User) Validate() *httperrors.HTTPError {
	if user.FirstName == "" {
		return httperrors.NewBadRequestError("invalid first name")
	}

	if user.LastName == "" {
		return httperrors.NewBadRequestError("invalid last name")
	}

	if user.Email == "" {
		return httperrors.NewBadRequestError("invalid email address")
	}

	return nil
}
