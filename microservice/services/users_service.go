package services

import (
	"fmt"
	"user-service/microservice/domain/httperrors"
	"user-service/microservice/domain/users"
)

var (
	// UsersService ...
	UsersService          = usersService{}
	registeredUsers       = map[int64]*users.User{}
	currentUserID   int64 = 1
)

type usersService struct{}

func (service usersService) Get(userID int64) (*users.User, *httperrors.HTTPError) {
	if user := registeredUsers[userID]; user != nil {
		return user, nil
	}

	return nil, httperrors.NewNotFouncError(fmt.Sprintf("user %d not found", userID))
}

// Create ...
func (service usersService) Create(user users.User) (*users.User, *httperrors.HTTPError) {

	if err := user.Validate(); err != nil {
		return nil, err
	}

	user.ID = currentUserID
	currentUserID++
	registeredUsers[user.ID] = &user

	return &user, nil
}

// Update ...
func (service usersService) Update(user users.User) *httperrors.HTTPError {
	if err := user.Validate(); err != nil {
		return err
	}

	if int(user.ID) > len(registeredUsers) {
		return httperrors.NewNotFouncError(fmt.Sprintf("user %d not found", user.ID))
	}

	registeredUsers[user.ID] = &user
	return nil
}

func (service usersService) Delete(userID int64) *httperrors.HTTPError {
	if user := registeredUsers[userID]; user != nil {
		delete(registeredUsers, userID)
		return nil
	}

	return httperrors.NewNotFouncError(fmt.Sprintf("user %d not found", userID))
}
