package aws

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"user-service/microservice/accounts"
	"user-service/microservice/domain/httperrors"
	"user-service/microservice/domain/users"

	"github.com/gin-gonic/gin"
)

const (
	domain = "https://search-revex-user-service-gm6h5mm2neq3hxsr25bauuichq.us-east-1.es.amazonaws.com"
	index  = "user"
)

var (
	// UsersService ...
	UsersService          = usersService{}
	registeredUsers       = map[int64]*users.User{}
	currentUserID   int64 = 1
)

type usersService struct{}

// Match ...
type Match struct {
	Query  string   `json:"query"`
	Fields []string `json:"fields"`
}

// Get ...
func Get(c *gin.Context) (*users.User, *httperrors.HTTPError) {
	userID := c.Param("id")
	// Basic information for the Amazon Elasticsearch Service domain
	endpoint := domain + "/" + index + "/" + "_search" + "/" + "?q=" + userID
	client := &http.Client{}

	// Form the HTTP request
	req, err := http.NewRequest(http.MethodGet, endpoint, nil)
	basicAuth(c, req)

	if err != nil {
		httpError := httperrors.HTTPError{
			Message: "invalid authorization",
			Code:    http.StatusUnauthorized,
			Error:   "Unauthorized",
		}
		return nil, &httpError
	}

	resp, err := client.Do(req)
	if err != nil {
		return nil, httperrors.NewNotFouncError("invalid user id")
	}

	bodyText, err := ioutil.ReadAll(resp.Body)

	var jsonResult map[string]interface{}
	err = json.Unmarshal(bodyText, &jsonResult)
	if err != nil {
		fmt.Println("Unmarshal fail")
	}

	hits := jsonResult["hits"].(map[string]interface{})
	insideHits := hits["hits"].([]interface{})
	element := insideHits[0].(map[string]interface{})
	source := element["_source"].(map[string]interface{})
	id, _ := strconv.ParseInt(source["id"].(string), 10, 64)

	user := users.User{
		ID:        id,
		FirstName: source["first_name"].(string),
		LastName:  source["last_name"].(string),
		Email:     source["email"].(string),
	}

	return &user, nil
}

func basicAuth(c *gin.Context, req *http.Request) {
	account := accounts.GetJSONInfo()

	accounts := gin.Accounts{
		account.User: account.Password,
	}

	secrets := gin.H{account.User: account.Email}

	user := c.MustGet(gin.AuthUserKey).(string)

	if _, ok := secrets[user]; !ok {
		c.JSON(http.StatusOK, gin.H{"message": "invalid authorization"})
		return
	}

	req.SetBasicAuth(user, accounts[user])
}
